Meteor.subscribe('imgs');

Handlebars.registerHelper('ifRouteIs', function (routeName, options) { 
  if (Meteor.Router.page() === routeName) {
    return options.fn(this);
  }
  return options.inverse(this);
});
