Template.imgPage.events({
	'submit form': function(e)
	{
		e.preventDefault();
		var currentImgId = this._id;
		var imgProperties = {
			name: $(e.target).find('[name=name]').val(),
			type: $(e.target).find('[name=type]').val(),
			page: $(e.target).find('[name=page]').val(),
			dim: $(e.target).find('[name=dim]').val(),
			path: $(e.target).find('[name=path]').val()
		}
		Imgs.update(currentImgId, {$set: imgProperties}, function(error) {
		if (error)
		{
			// display the error to the user
			throwError(error.reason);
		}
		else
		{
			Router.go('imgPage', {_id: currentImgId}); }
		});
	},
	'click .delete': function(e)
	{
		e.preventDefault();
		if (confirm("Delete this post?"))
		{
			var currentImgId = this._id; Imgs.remove(currentImgId);
			Router.go('all');
		}
	}
});
