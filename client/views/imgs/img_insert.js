Template.imgInsert.events({
		'submit form': function(e) {
			e.preventDefault();
			var img = {
				name: $(e.target).find('[name=name]').val(),
				page: $(e.target).find('[name=page]').val(),
				type: $(e.target).find('[name=type]').val(),
				dim: $(e.target).find('[name=dim]').val(),
				path: $(e.target).find('[name=path]').val()
			}
			img._id = Imgs.insert(img);
			Router.go('imgPage', img);
		}
});
