Template.homeList.helpers({
		imgs: function()
		{
			return Imgs.find({page: "home"}).fetch();
		}
});

Template.homeImg.helpers({
		domain: function() {
		var a = document.createElement('a');
		return a;
} });

Template.homeImg.rendered = function () {
	$('.home-imgs').slick({
	  autoplay: true,
	  speed: 1900,
	  fade: true,
	});
};
