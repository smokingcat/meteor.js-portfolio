# Simple Portfolio #
---

This is a simple responsive [porfolio](http://www.clementine-chabaneix.com/) I made for my sister.

The specifications were pretty straightforward:

* Responsive
* Simple design
* Ability to add and modify the content
* Mailchimp integration for the mailing list
* Google analytics ready 

### Oreview ###
Homepage (users can click on links and directions):
![Screen Shot 2015-03-03 at 18.05.37.png](https://bitbucket.org/repo/qoLnpp/images/1395353678-Screen%20Shot%202015-03-03%20at%2018.05.37.png)
Mobile Ready:

![Screen Shot 2015-03-03 at 18.06.35.png](https://bitbucket.org/repo/qoLnpp/images/2926747090-Screen%20Shot%202015-03-03%20at%2018.06.35.png)

Add an image:

![Screen Shot 2015-03-03 at 18.28.10.png](https://bitbucket.org/repo/qoLnpp/images/756293864-Screen%20Shot%202015-03-03%20at%2018.28.10.png)

Update an image:


![Screen Shot 2015-03-03 at 18.26.09.png](https://bitbucket.org/repo/qoLnpp/images/2782561325-Screen%20Shot%202015-03-03%20at%2018.26.09.png)

Mailchimp intégration:

![Screen Shot 2015-03-03 at 18.08.01.png](https://bitbucket.org/repo/qoLnpp/images/2973631887-Screen%20Shot%202015-03-03%20at%2018.08.01.png)