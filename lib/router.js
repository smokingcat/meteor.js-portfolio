Router.configure({ layoutTemplate: 'layout'
});


Router.map(function () {
  this.route('imgPage', {
	path: '/imgs/:_id',
	data: function() { return Imgs.findOne(this.params._id); }
  });
  this.route('about', {
    path: '/about'
  });
   this.route('homeList', {
    path: '/'
  });
   this.route('thanks', {
    path: '/thanks',
  });
  this.route('sculptures', {
    path: '/sculptures',
    template: 'scp',
  });
  this.route('dessins', {
    path: '/dessins'
  });
  this.route('contact', {
    path: '/contact'
  });
  this.route('installations', {
    path: '/installations'
  });
  this.route('all', {
    path: '/all'
  });
  this.route('admin', {
    path: '/admin'
  });
  this.route('ceramique', {
    path: '/sculptures/ceramique'
  });
  this.route('resine', {
    path: '/sculptures/resine'
  });
  this.route('imgInsert', {
	path: '/insert'
  });
});

